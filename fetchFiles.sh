#!/bin/bash

commit="5370b45ddaec255d325e4bf4e1c6aee0f3776871"
path="https://raw.githubusercontent.com/sunag/sea3d/${commit}/Labs/sea3d-1.8/"
files=("SEA3D.js" "SEA3DAnimation.js" "SEA3DNodeMaterial.js" "SEA3DLZMA.js" "SEA3DLoader.js")

echo "Fetching files from:"
echo ${path}

for item in ${files[*]}
do
	curl -O ${path}${item}
done

